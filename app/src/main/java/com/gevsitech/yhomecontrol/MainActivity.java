package com.gevsitech.yhomecontrol;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Socket;

public class MainActivity extends AppCompatActivity {

    static final String HOST_IP = "10.10.100.254";
    static final int PORT = 8899;
    InetSocketAddress inetSocketAddress;
    private Button Connect ;
    Socket socket;
    private Button sendButton ;
    private EditText editText;
    Thread thread;
    private  int datas = 100 ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Connect = (Button)findViewById(R.id.button) ;
        sendButton = (Button)findViewById(R.id.button2) ;
        editText = (EditText)findViewById(R.id.editText);

        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                System.out.println(keyCode);
                return false;
            }
        });

        inetSocketAddress = new InetSocketAddress(HOST_IP, PORT);

        Connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            socket = new Socket(HOST_IP, PORT);
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                        if(!socket.isConnected()){
                            try {
                                socket.connect(inetSocketAddress);
                                socket.isConnected();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            int k = 0 ;
                            boolean connectflag = false;
                            while (!socket.isConnected()){
                                connectflag  = socket.isConnected();
                                k++;
                                if(k>3000000){
                                    break;
                                }
                            }
                            if(connectflag){
                                System.out.println("連線成功");
                            }else{
                                System.out.println("連線失敗");

                            }
                        }else{
                            System.out.println("連線存在");
                        }


                    }
                });
                thread.start();
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("OK");
                editText.getText();
                if(thread!=null)thread.interrupt();
                thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if( socket == null){
                            try {
                                socket = new Socket(HOST_IP, PORT);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        datas = 100;
                        while (datas>0){
                            datas--;
                            byte[] data = new byte[0];
                            try {
                                Thread.sleep(10);
                                data =  (datas+"").getBytes("UTF-8");
                                OutputStream os = socket.getOutputStream();
                                os.write(data);
                            } catch (Exception e) {
                                e.printStackTrace();
                                System.out.println("發送失敗");
                                break;
                            }
                        }


                    }
                });
                thread.start();

            }
        });
    }



    //socket連線 做為用戶端

}
